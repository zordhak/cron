#!/usr/bin/perl -w

use strict;

# Usage: ./extract_lang.pl lang file
# where 'lang' is the 2-letter ISO abbreviation for the language and
# 'file' is the name of the file you wish to extract data from
# Example: ./extract_lang.pl es releases/potato
# will extract the list of bad links from the Spanish translation from
# the file releases/potato

my $lang = $ARGV[0];
my $file = $ARGV[1];
if (!$lang) {
	print "The first argument must be the 2-letter ISO language.";
	print "The second argument must be the file to work on";
	exit(1);
}
if (!$file) {
	die "The second argument must be a filename";
}

if (-f $file) {
	open(FIL, "<$file");
}
else {
	die "no such file, $file";
}
my $search = "^Looking into http:.*\.$lang\.html\n";

my $line = "";
my $section = "";
my $blank;
foreach (<FIL>) {
	if (/^Looking into/) {
		$line = $_;
		if ($section and not $blank) {
			process($section);
			$blank = 1;
		}
		$section = $line;
	}
	else {
		$section .= $_;
		$blank = 0;
	}
}
if ($section and not $blank) {
    process($section);
}

sub process {
	my ($section) = @_;

	if ($section =~ m,^Looking into http://[\w/.-]+\n$,m) {
		return;
	}
	if ($section =~ m,$search,) {
		print $section;
	}
	
}
